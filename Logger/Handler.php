<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    /**
     * @var int
     */
    protected $loggerType = \Monolog\Logger::INFO;

    /**
     * @var string
     */
    protected $fileName = '/var/log/send-order-to-google-analytics.log';

}
