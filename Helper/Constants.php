<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Helper;

class Constants
{

    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_DEFAULT_TEST_URL = 'https://www.google-analytics.com/debug/collect';
    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_DEFAULT_PRODUCTION_URL = 'https://www.google-analytics.com/collect';

    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_ACTIVE_PATH = 'google/send_order_to_google_analytics/active';
    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_MODE_PATH = 'google/send_order_to_google_analytics/mode';
    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_TEST_URL_PATH = 'google/send_order_to_google_analytics/test_url';
    const GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_PRODCUTION_URL_PATH = 'google/send_order_to_google_analytics/prodcution_url';

    const REQUEST_DATA_ORDER_TRANSACTION_TYPE = 'transaction';
    const REQUEST_DATA_ITEM_ORDER_TRANSACTION_TYPE = 'item';

    const DEFAULT_HTTP_USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';

    const AFFILIATION_STORE_AREA = 'store';
    const AFFILIATION_ADMIN_AREA = 'admin';

}
