<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Helper;

class SystemConfiguration
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\GoogleAnalytics\Helper\Data
     */
    protected $googleAnalyticsHelper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\GoogleAnalytics\Helper\Data $googleAnalyticsHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->googleAnalyticsHelper = $googleAnalyticsHelper;
    }

    /**
     * @param $store
     * @return bool
     */
    public function isActive($store = null): bool
    {
        $isGoogleAnalyticsAvailable = $this->googleAnalyticsHelper->isGoogleAnalyticsAvailable($store);
        if (!$isGoogleAnalyticsAvailable) {
            return false;
        }

        $value = $this->scopeConfig->getValue(
            \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_ACTIVE_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return ($value) ? true : false;
    }

    /**
     * @return bool
     */
    public function isProductionMode(): bool
    {
        $value = $this->scopeConfig->getValue(
            \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_MODE_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return ($value) ? true : false;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        $isProductionMode = $this->isProductionMode();
        if (!$isProductionMode) {
            return $this->getTestUrl();
        }

        return $this->getProductionUrl();
    }

    /**
     * @return string
     */
    public function getTestUrl(): string
    {
        $value = $this->scopeConfig->getValue(
            \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_TEST_URL_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return ($value) ? $value : \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_DEFAULT_TEST_URL;
    }

    /**
     * @return string
     */
    public function getProductionUrl(): string
    {
        $value = $this->scopeConfig->getValue(
            \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_PRODCUTION_URL_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return ($value) ? $value : \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::GOOGLE_SEND_ORDER_TO_GOOGLE_ANALYTICS_DEFAULT_PRODUCTION_URL;
    }

    /**
     * @param $store
     * @return string
     */
    public function getGoogleAnalyticsAccount($store = null): string
    {
        return $this->scopeConfig->getValue(
            \Magento\GoogleAnalytics\Helper\Data::XML_PATH_ACCOUNT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

}
