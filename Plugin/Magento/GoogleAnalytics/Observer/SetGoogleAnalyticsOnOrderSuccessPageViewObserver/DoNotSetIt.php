<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Plugin\Magento\GoogleAnalytics\Observer\SetGoogleAnalyticsOnOrderSuccessPageViewObserver;

class DoNotSetIt
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration $systemConfiguration
    )
    {
        $this->logger = $logger;
        $this->systemConfiguration = $systemConfiguration;
    }

    public function aroundExecute(\Magento\GoogleAnalytics\Observer\SetGoogleAnalyticsOnOrderSuccessPageViewObserver $subject, callable $proceed, \Magento\Framework\Event\Observer $observer)
    {
        $this->logger->info('Executing DoNotSetIt plugin - do_not_set_google_analytics_on_order_success_page');

        $isActive = $this->systemConfiguration->isActive();
        if (!$isActive) {
            return $proceed($observer);
        }

        return;
    }

}
