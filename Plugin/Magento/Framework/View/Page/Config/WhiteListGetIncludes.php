<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Plugin\Magento\Framework\View\Page\Config;

class WhiteListGetIncludes
{

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $httpRequest;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    public function __construct(
        \Magento\Framework\App\Request\Http $httpRequest,
        \Psr\Log\LoggerInterface $logger,
        \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration $systemConfiguration
    )
    {
        $this->httpRequest = $httpRequest;
        $this->logger = $logger;
        $this->systemConfiguration = $systemConfiguration;
    }

    public function afterGetIncludes(\Magento\Framework\View\Page\Config $subject, $result)
    {
        $isActive = $this->systemConfiguration->isActive();
        $fullActionName = $this->httpRequest->getFullActionName();
        if ($fullActionName == 'checkout_onepage_success' AND $isActive) {
            $result = '';
        }

        return $result;
    }

}
