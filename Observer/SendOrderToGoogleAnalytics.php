<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Observer;

class SendOrderToGoogleAnalytics implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Service\SendOrderToGoogleAnalytics
     */
    protected $sendOrderToGoogleAnalyticsRequest;

    public function __construct(
        \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration $systemConfiguration,
        \Agrekom\SendOrderToGoogleAnalytics\Service\SendOrderToGoogleAnalyticsRequest $sendOrderToGoogleAnalyticsRequest
    ) {
        $this->systemConfiguration = $systemConfiguration;
        $this->sendOrderToGoogleAnalyticsRequest = $sendOrderToGoogleAnalyticsRequest;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $isActive = $this->systemConfiguration->isActive();
        if (!$isActive) {
            return;
        }

        $order = $observer->getOrder();
        $this->sendOrderToGoogleAnalyticsRequest->execute($order->getIncrementId(), $order->getStoreId());
    }

}
