<?php

namespace Agrekom\SendOrderToGoogleAnalytics\Service;

class SendOrderToGoogleAnalyticsRequest
{

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $order;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlHttpClient;

    /**
     * @var \Magento\Framework\HTTP\Header
     */
    protected $httpHeader;

    /**
     * @var \Jflight\GACookie\GACookie
     */
    protected $GACookie;

    public function __construct(
        \Agrekom\SendOrderToGoogleAnalytics\Logger\Logger $logger,
        \Magento\Sales\Model\Order $order,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Agrekom\SendOrderToGoogleAnalytics\Helper\SystemConfiguration $systemConfiguration,
        \Magento\Framework\App\State $state,
        \Magento\Framework\HTTP\Client\Curl $curlHttpClient,
        \Magento\Framework\HTTP\Header $httpHeader,
        \Jflight\GACookie\GACookie $GACookie
    ) {
        $this->logger = $logger;
        $this->order = $order;
        $this->storeManager = $storeManager;
        $this->systemConfiguration = $systemConfiguration;
        $this->state = $state;
        $this->curlHttpClient = $curlHttpClient;
        $this->httpHeader = $httpHeader;
        $this->GACookie = $GACookie;
    }

    public function execute($incrementId, $storeId)
    {
        $this->logger->info(
            sprintf("Processing order %s ... ", $incrementId)
        );

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->order->loadByIncrementIdAndStoreId($incrementId, $storeId);
        if (!$order->getId()) {
            $this->logger->error(
                sprintf("Can not find order with increment ID %s", $incrementId)
            );
        }

        $store = $this->getStore($incrementId, $storeId);
        if (!$store) {
            $this->logger->error(
                sprintf("Can not find store (%s) for order increment ID %s", $storeId, $incrementId)
            );
        }

        $affiliation = $this->getAffiliation();
        if (!$affiliation) {
            $this->logger->error(
                sprintf("Can not find store area code for order increment ID %s. There for I can not create affiliation code.", $incrementId)
            );
        }

        $requestOrderData = $this->prepareOrderRequestData($order, $store, $affiliation);
        $orderDataJson = json_encode($requestOrderData);
        $this->logger->info(
            sprintf("Order %s data: %s", $incrementId, $orderDataJson)
        );
        $request = $this->request($requestOrderData);
        if ($request) {
            /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
            foreach ($order->getAllVisibleItems() as $item) {
                $orderItemRequestData = $this->prepareOrderItemRequestData($order, $item, $store, $affiliation);
                $orderItemDataJson = json_encode($orderItemRequestData);
                $this->logger->info(
                    sprintf("Item for order %s data: %s", $incrementId, $orderItemDataJson)
                );
                $this->request($orderItemRequestData);
            }
        }
    }

    /**
     * @param array $requestData
     * @return bool
     */
    protected function request(array $requestData)
    {
        $url = $this->systemConfiguration->getUrl();
        $httpQuery = http_build_query($requestData);

        $requestUrl = sprintf("%s?%s", $url, $httpQuery);
        $userAgent = $this->getUserAgent();

        $curlOptions = [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_USERAGENT => $userAgent
        ];

        try {
            $this->curlHttpClient->setOptions($curlOptions);
            $this->curlHttpClient->get($requestUrl);
            $response = $this->curlHttpClient->getBody();
            if ($this->curlHttpClient->getStatus() != 200) {
                $this->logger->error(
                    sprintf("Data was not send to Google Analytics. Wrong response code %s for order increment ID %s", $this->curlHttpClient->getStatus(), $requestData['ti'])
                );

                return false;
            }

            if ($requestData['t'] == \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::REQUEST_DATA_ORDER_TRANSACTION_TYPE) {
                $this->logger->info(
                    sprintf("Order %s processed with body response %s", $requestData['ti'], $response)
                );
            } else {
                $this->logger->info(
                    sprintf("Item from order %s processed with body response %s", $requestData['ti'], $response)
                );
            }

        } catch (\Exception $exception) {
            $this->logger->error(
                sprintf("Data was not send to Google Analytics. Error found with messasge %s in file %s and line %s for order increment ID %s", $exception->getMessage(), $exception->getFile(), $exception->getLine(), $requestData['ti'])
            );

            return false;
        }

        return true;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Sales\Api\Data\OrderItemInterface $item
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string $affiliation
     * @return array
     */
    protected function prepareOrderItemRequestData(
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Api\Data\OrderItemInterface $item,
        \Magento\Store\Api\Data\StoreInterface $store,
        string $affiliation
    ) {
        $gaId = $this->systemConfiguration->getGoogleAnalyticsAccount($store);

        $ti = $order->getIncrementId();
        if (!$this->systemConfiguration->isProductionMode()) {
            $ti = sprintf("%s-%s", 'TEST', $ti);
        }

        $orderItemRequestData = [];
        $orderItemRequestData['v'] = 1;
        $orderItemRequestData['tid'] = $gaId;
        $orderItemRequestData['cid'] = $order->getCustomerEmail();
        $orderItemRequestData['t'] = \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::REQUEST_DATA_ITEM_ORDER_TRANSACTION_TYPE;
        $orderItemRequestData['ti'] = $ti;
        $orderItemRequestData['in'] = $item->getName();
        $orderItemRequestData['ip'] = $item->getPrice();
        $orderItemRequestData['iq'] = $item->getQtyOrdered();
        $orderItemRequestData['ic'] = $item->getSku();
        $orderItemRequestData['cu'] = $order->getBaseCurrencyCode();

        $utmz = $this->GACookie::parse('utmz');
        if (isset($utmz->campaign)) {
            $orderItemRequestData['cn'] = $utmz->campaign;
        }
        if (isset($utmz->source)) {
            $orderItemRequestData['cs'] = $utmz->source;
        }
        if (isset($utmz->medium)) {
            $orderItemRequestData['cm'] = $utmz->medium;
        }

        return $orderItemRequestData;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @param string $affiliation
     * @return array
     */
    protected function prepareOrderRequestData(
        \Magento\Sales\Model\Order $order,
        \Magento\Store\Api\Data\StoreInterface $store,
        string $affiliation
    ) {
        $gaId = $this->systemConfiguration->getGoogleAnalyticsAccount($store);

        $ti = $order->getIncrementId();
        if (!$this->systemConfiguration->isProductionMode()) {
            $ti = sprintf("%s-%s", 'TEST', $ti);
        }

        $orderRequestData = [];
        $orderRequestData['v'] = 1;
        $orderRequestData['tid'] = $gaId;
        $orderRequestData['cid'] = $order->getCustomerEmail();
        $orderRequestData['t'] = \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::REQUEST_DATA_ORDER_TRANSACTION_TYPE;
        $orderRequestData['ta'] = $affiliation;
        $orderRequestData['ti'] = $ti;
        $orderRequestData['tr'] = $order->getGrandTotal();
        $orderRequestData['ts'] = $order->getShippingAmount();
        $orderRequestData['tt'] = $order->getTaxAmount();
        $orderRequestData['cu'] = $order->getBaseCurrencyCode();

        $utmz = $this->GACookie::parse('utmz');
        if (isset($utmz->campaign)) {
            $orderRequestData['cn'] = $utmz->campaign;
        }
        if (isset($utmz->source)) {
            $orderRequestData['cs'] = $utmz->source;
        }
        if (isset($utmz->medium)) {
            $orderRequestData['cm'] = $utmz->medium;
        }

        return $orderRequestData;
    }

    /**
     * @return string
     */
    protected function getUserAgent()
    {
        $httpUserAgent =  $this->httpHeader->getHttpUserAgent();
        if (!$httpUserAgent) {
            return \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::DEFAULT_HTTP_USER_AGENT;
        }

        return $httpUserAgent;
    }

    /**
     * @return string|null
     */
    protected function getAffiliation()
    {
        try {
            $areaCode = $this->state->getAreaCode();
            if ($areaCode != \Magento\Framework\App\Area::AREA_ADMINHTML) {
                return \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::AFFILIATION_STORE_AREA;
            }

            return \Agrekom\SendOrderToGoogleAnalytics\Helper\Constants::AFFILIATION_ADMIN_AREA;
        } catch (\Magento\Framework\Exception\LocalizedException $localizedException) {
        }

        return null;
    }

    /**
     * @param $incrementId
     * @param $storeId
     * @return \Magento\Store\Api\Data\StoreInterface|null
     */
    protected function getStore($incrementId, $storeId)
    {
        try {
            return $this->storeManager->getStore($storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
        }

        return null;
    }

}
