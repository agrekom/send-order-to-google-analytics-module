<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Agrekom_SendOrderToGoogleAnalytics',
    __DIR__
);